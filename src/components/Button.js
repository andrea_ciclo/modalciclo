import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

export default class Button extends Component {
    render() {
        return (
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#miModal">
                Abrir modal
            </button>
        )
    }
}