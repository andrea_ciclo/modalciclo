import React, { Component } from 'react';
import Modal, { ModalHeader, ModalBody, ModalFooter } from './components/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';
import img1 from '../src/imagenes/img1.jpg'
import img2 from '../src/imagenes/img2.jpg'
import img3 from '../src/imagenes/img3.jpg'
import './App.css';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ modal: !this.state.modal });
  }

  render() {
    return (
      <div className="App" >
        <h1>Ejemplo de Modal</h1>
        <div id="buttoncontainer">
        <button id="button1"
          type="button"
          className="btn btn-light"
          onClick={this.toggle}
          >Click</button>
        </div>
        
          <Modal id="modalcontainer" isOpen={this.state.modal} >
          <ModalBody>
          <div id="h3">
            <h5 id="titulo">¿Qué envases son válidos?</h5>
            </div>
            <div className="body">
              <img src={img1} id="img1" className="img-fluid"></img>
              <img src={img2} id="img2" className="img-fluid"></img>
              <img src={img3} id="img3" className="img-fluid"></img>
            </div>
            <div className="textbody">
            <p id="textos">1. Ingresa tu número de cédula</p>
            <p id="textos">2. Sostén el envase y escanea el código</p>
            <p id="textos">3. Empuja el envase</p>
            </div>
            <div className="finalcontenedor">
              <p className="final">
                * Siempre y cuando tengan su código de barras, <br/>
                  conserven su forma y estén limpios y secos.
                  <hr className="hr" />
              </p>
            </div>
          </ModalBody>
            <div id="buttoncontainer2">
            <button
            id="buttonmodal"
              type="button"
              className="btn btn-light"
              // className="btn btn-success"
              onClick={this.toggle}>¡LISTO!</button>
            </div>
            <hr/>
        </Modal>
        </div>
    );
  }
}

export default App;